#!/bin/bash
PATH=/etc:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

# Определенная директория файла
DIR=/tmp/test/files

# Лог файла
LOGFILE=/tmp/test/script-log/sync.log

# Текущая дата
DATE=`date +%d_%m_%Y`

# Архив в папку
ARCHIVE_DIR=/tmp/test/archives

# Скрипт должен по расписанию выбирать последний по дате изменения файлов в определенной директории /tmp/test/files не старее 7 дней
FILES=`find $DIR -type f -mtime -7 -printf "%T@ %k %p\n" | sort -n | head -2 | cut -d' ' -f2-3`

read size1 path1 size2 path2 <<< $FILES

# Для выбранного файла записывать (добавлять в конец) в лог файл /tmp/test/script-log/sync.log информацию о текущей дате, пути к файлу, его имени, и объему
echo "Дата:"$DATE "размер:"$size1 "имя:"$path1 >> $LOGFILE 
echo "Дата:"$DATE "размер:"$size2 "имя:"$path2 >> $LOGFILE

# Сжимать файл в отдельный архив .tar.gz с тем же именем, что и исходный файл и датой и временем создания архива
# Перемещать архив в папку /tmp/test/archives
# В случае успешного перемещения – удалять исходный файл из /tmp/test/files
tar -cvzf $ARCHIVE_DIR($basename $path1).tgz $path1 --remove-files
